build: 
     docker build -t searx:latest .

run: build
    docker run -p 8080:8080 searx:latest
